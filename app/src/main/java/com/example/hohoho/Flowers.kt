package com.example.hohoho

data class Person(
    val id: Int,
    val title: String,
    val name: String,
    val imageUrl: String,
) {
}
