package com.example.insertphotoactivity

import java.net.URL

data class UserInfo(
    val name: String = "",
    val url: String = ""
)
